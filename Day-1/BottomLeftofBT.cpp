// LEETCODE 513. Find Bottom Left Tree Value

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int findBottomLeftValue(TreeNode* root) {
        vector<vector<int>> res;
        if(root == NULL)
            return -1;
        queue<TreeNode*> q;
        q.push(root);
        while(!q.empty()){
            vector<int> level;
            int n = q.size();
            for(int i = 0; i < n; i++){
                TreeNode *node = q.front();
                q.pop();
                if(node -> left != NULL)
                    q.push(node -> left);
                if(node -> right != NULL)
                    q.push(node -> right);
                
                level.push_back(node -> val);
            }
            res.push_back(level);
        }
        
        return res[res.size() - 1][0]; // last level, first node value
    }
};
