// GFG

class Solution {
  public:
    vector <int> bottomView(Node *root) {
        // Your Code Here
        vector<int> res;
        if(root == NULL){
            return res;
        }
        map<int, int> mp;
        queue<pair<Node*, int>> q;
        q.push({root, 0});
        
        while(!q.empty()){
            int n = q.size();
            for(int i = 0; i < n; i++){
                auto it = q.front();
                q.pop();
                Node* temp = it.first;
                int horizontal = it.second;
                
                if(temp -> left){
                    q.push({temp -> left, horizontal - 1});
                }
                if(temp -> right){
                    q.push({temp -> right, horizontal + 1});
                }
                    mp[horizontal] = temp -> data;
            }
        }
        for(auto item : mp){
            res.push_back(item.second);
        }
        return res;
    }
};
