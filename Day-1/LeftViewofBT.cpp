// GFG

/* A binary tree node

struct Node
{
    int data;
    struct Node* left;
    struct Node* right;
    
    Node(int x){
        data = x;
        left = right = NULL;
    }
};
 */

//Function to return a list containing elements of left view of the binary tree.
vector < int > leftView(Node * root) {
  // Your code here
  vector < int > res;
  if (root == NULL) {
    return res;
  }

  queue < Node * > q;
  q.push(root);

  while (!q.empty()) {
    int n = q.size();
    vector < int > level;
    for (int i = 0; i < n; i++) {
      Node * temp = q.front();
      q.pop();
      if (temp -> left != NULL) {
        q.push(temp -> left);
      }
      if (temp -> right != NULL) {
        q.push(temp -> right);
      }
      level.push_back(temp -> data);
    }
    res.push_back(level[0]);
  }
  return res;
}
