// GFG

/*   
struct Node
{
    int data;
    Node* left;
    Node* right;
}; */
vector<int> reverseLevelOrder(Node *root)
{
    // code here
    vector<int> res;
    if(root == NULL){
        return res;
    }
    queue<Node*> q;
    stack<Node*> st;
    q.push(root);
    
    while(!q.empty()){
        int n = q.size();
        for(int i = 0; i < n; i++){
            Node *temp = q.front();
            q.pop();
            
            if(temp -> right != NULL){
                q.push(temp -> right);
            }
            if(temp -> left != NULL){
                q.push(temp -> left);
            }
            
            st.push(temp);
        }
    }
    
    while(!st.empty()){
        Node *temp = st.top();
        st.pop();
        res.push_back(temp -> data);
    }
    
    return res;
}
