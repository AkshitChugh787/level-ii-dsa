// GFG

/*
struct Node
{
    int data;
    Node* left;
    Node* right;
};
*/
class Solution
{
    public:
    //Function to return a list of nodes visible from the top view 
    //from left to right in Binary Tree.
    vector<int> topView(Node *root)
    {
        vector<int> res;
        if(root == NULL){
            return res;
        }
        //Your code here
        map<int, int> mp;
        queue<pair<Node*, int>> q;
        q.push({root, 0});
        
        while(!q.empty()){
            int n = q.size();
            for(int i = 0; i < n; i++){
                auto it = q.front();
                q.pop();
                Node* temp = it.first;
                int horizontal = it.second;
                
                if(temp -> left){
                    q.push({temp -> left, horizontal - 1});
                }
                if(temp -> right){
                    q.push({temp -> right, horizontal + 1});
                }
                
                if(mp.find(horizontal) == mp.end()){
                    mp[horizontal] = temp -> data;
                }
            }
        }
        for(auto item : mp){
            res.push_back(item.second);
        }
        return res;
    }

};
